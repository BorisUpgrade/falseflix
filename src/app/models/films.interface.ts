
export interface ImageInterface{
    title: string;
    src: string;
}

export interface FilmsInterface{
    section: string,
    film: ImageInterface[];
}
//export interface ImageInterface{    src: string;    alt?: string;}