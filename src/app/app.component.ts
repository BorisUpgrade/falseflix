import { FilmsInterface } from './models/films.interface';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'netflix-app';

  public comedyFilms: FilmsInterface = {
    section: "Comedia",
    film: [{
      title: "Casi 300",
      src: "../assets/images/Comedia/casi300.webp"
    },{
      title: "El Dictador",
      src: "../assets/images/Comedia/cazafantasmas.webp"
    },{
      title: "Erase una vez en hollywood",
      src: "../assets/images/Comedia/erasehollywood.webp"
    },{
      title: "Padre de familia",
      src: "../assets/images/Comedia/family-guy.webp"
    },{
      title: "Juerga",
      src: "../assets/images/Comedia/juerga.webp"
    },]
  }
  public animeFilms: FilmsInterface = {
    section: "Anime",
    film: [{
      title: "Ajin",
      src: "../assets/images/Anime/ajin.jpg"
    },{
      title: "Castillo Ambulante",
      src: "../assets/images/Anime/castilloambulante.webp"
    },{
      title: "El viaje de Chihiro",
      src: "../assets/images/Anime/chihiro.webp"
    },{
      title: "Evangelion",
      src: "../assets/images/Anime/evangelion.webp"
    },{
      title: "Git a Rise",
      src: "../assets/images/Anime/gitsarise.webp"
    },]
  }

  public topFilms: FilmsInterface = {
    section: "Top",
    film: [{
      title: "La casa de la broza",
      src: "../assets/images/Top10/1-papel.webp"
    },{
      title: "Reina",
      src: "../assets/images/Top10/2-reina.webp"
    },{
      title: "Titanes",
      src: "../assets/images/Top10/3-titanes.webp"
    },{
      title: "Lost in space",
      src: "../assets/images/Top10/4-lostinspace.webp"
    },{
      title: "Donde Caben",
      src: "../assets/images/Top10/5-dondecaben.webp"
    },]
  }
}
